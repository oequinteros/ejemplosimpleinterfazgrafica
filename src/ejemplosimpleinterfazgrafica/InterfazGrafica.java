package ejemplosimpleinterfazgrafica;

import interfazgrafica.Principal;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;

public class InterfazGrafica {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame aplicacion = new Principal("Ejemplo Controles Swing");
        aplicacion.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        

        Dimension dim_pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dim_cuadro = aplicacion.getSize();
        aplicacion.setLocation((dim_pantalla.width-dim_cuadro.width)/2,(dim_pantalla.height-dim_cuadro.height)/2);        
        
//        Toolkit toolkit = Toolkit.getDefaultToolkit();
//        Dimension dimension = toolkit.getScreenSize();        
//        aplicacion.setSize(dimension);        

        aplicacion.setVisible(true); 
        
    }
    
}
