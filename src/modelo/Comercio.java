/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author oscarquinteros
 */
public class Comercio {
    private ArrayList<Producto> productos = new ArrayList<Producto>();
    private ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    private ArrayList<CondicionIVA> parametrosIva = new ArrayList<CondicionIVA>(); 
    
    private static Comercio comercio;
    
    private Comercio(){
        
    }
    
    public static Comercio instancia(){
        if (comercio==null){
            comercio=new Comercio();
        }
        return comercio;
    }
    
    public ArrayList<Producto> getProductos(){
        return productos;
    }
    public ArrayList<Cliente> getClientes(){
        return clientes;
    }
    public ArrayList<CondicionIVA> getParametrosIva(){
        return parametrosIva;
    }
    
}
